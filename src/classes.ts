abstract class Department {
    protected employees: string[] = [];

    constructor(protected readonly id: string, public name: string) {
    }

    static createEmployee(name: string) {
        return {name};
    }

    abstract describe(): void;

    addEmployee(employee: string) {
        this.employees.push(employee);
    }

    printEmployeeInformation() {
        console.log(this.employees);
    }
}

class ITDepartment extends Department {
    constructor(id: string, public admins: string[]) {
        super(id, 'IT');
    }

    describe() {
        console.log('It department - id: ', this.id);
    }
}

class AccountingDepartment extends Department {
    private static instance: AccountingDepartment;

    private lastReport: string;

    get mostRecentReport() {
        if(this.lastReport) {
            return this.lastReport;
        }

        throw new Error('No report found'); 
    }

    set mostRecentReport(value: string) {
        this.addReport(value);
    }
    
    private constructor(id: string, private reports: string[]) {
        super(id, 'Accounting');
        this.lastReport = reports[0];
    }

    static getInstance() {
        if(AccountingDepartment.instance) {
            return this.instance
        }

        this.instance = new AccountingDepartment('d2', []);
        return this.instance;
    }

    addEmployee(name: string) {
        if(name === 'Max') {
            return;
        }

        this.employees.push(name);
    }

    addReport(text: string) {
        this.reports.push(text);
    }

    describe() {
        console.log('Accounting deparmtne id: ', this.id);
    }

    printReports() {
        console.log(this.reports);
    }
}

const employee1 = Department.createEmployee('Max');

const accounting = AccountingDepartment.getInstance();

console.log(accounting.mostRecentReport)
